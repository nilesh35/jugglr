import { Component, OnInit, HostListener } from '@angular/core';
import { JugglrServices } from '../../Services/JugglrServices';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../models/user';

import { from } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { PaymentServiceService } from 'src/app/payment-service.service';

declare var stripe: any;


@Component({
  selector: 'app-business-premium',
  templateUrl: './business-premium.component.html',
  styleUrls: ['./business-premium.component.css'],

})
export class BusinessPremiumComponent implements OnInit {
  message: string;

  constructor(private paymentSvc: PaymentServiceService,private http: HttpClient, private route: ActivatedRoute, private router: Router, private modal: NgbModal, private jservices: JugglrServices, private currentuser: User) {

  }
  handler: any;
  amount = 3000;


  ngOnInit() {
    this.handler = StripeCheckout.configure({
      key: environment.stripeKey,
      image: '/assets/iconblue.png',
      locale: 'auto',
      token: token => {
        console.log(this.handler.stripeEmail);
        this.paymentSvc.processPayment(token, this.amount,"", undefined)
      },
      stripeEmail:email=>{console.log(email)}

    });
    const profile = JSON.parse(localStorage.getItem('currentUser'));

    const userid = profile['id'].toString();
    const usertoken = profile['user_token'];
    const devicetype = profile['device_type'];
    this.currentuser.myconstructor(profile);

    console.log(this.currentuser)
  }

  handlePayment() {
    this.handler.open({
      name: 'Jugglr Business Premium',
      excerpt: 'Deposit Funds to Account',
      description:'40% off, $30 per month',
      amount: this.amount,
      interval:'monthly',
      currency:'aud'


    });
  }
  @HostListener('window:popstate')
    onPopstate() {
      this.handler.close()
    }



  someMethod() {

    //this.subscribehttp().subscribe(data => {});
  }



}
