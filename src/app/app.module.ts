import { BrowserModule } from '@angular/platform-browser';
import { JugglrServices } from './Services/JugglrServices';
import { S3UploadFileService } from './Services/S3UploadService';

import { Job } from './models/job';
import { Offer } from './models/offer';

import { User } from './models/user';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ROUTING } from './app.routing';
import { DlDateTimePickerDateModule } from 'angular-bootstrap-datetimepicker';
import {NgbModule, NgbRatingConfig} from '@ng-bootstrap/ng-bootstrap';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularFireStorageModule } from '@angular/fire/storage';

import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { AppComponent } from './app.component';
import { HeadlineComponent } from './views/headline/headline.component';
import { HeaderComponent } from './views/header/header.component';
import { SponsorsComponent } from './views/sponsors/sponsors.component';
import { ServicesComponent } from './views/services/services.component';
import { JobsComponent } from './views/jobs/jobs.component';
import { HomeComponent } from './views/home/home.component';
import { LoginComponent } from './views/login/login.component';
import { SocialloginComponent } from './views/sociallogin/sociallogin.component';
import { AddjobformComponent } from './views/addjobform/addjobform.component';
import * as moment from 'moment/moment';
import { ChartModule } from 'angular2-chartjs';
import {CalendarModule} from 'primeng/calendar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  SocialLoginModule,
  AuthService,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
  LinkedinLoginProvider,
} from "angular-6-social-login";
import { JoblistComponent } from './views/joblist/joblist.component';
import { JobdetailComponent } from './views/jobdetail/jobdetail.component';
import { ModalConfigComponent } from './utils/modal-config/modal-config.component';
import { DashboardMenuComponent } from './views/dashboard/dashboard-menu/dashboard-menu.component';
import { DashboardContentComponent } from './views/dashboard/dashboard-content/dashboard-content.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { MarketplaceComponent } from './views/dashboard/dashboard-content/marketplace/marketplace.component';
import { BookingsComponent } from './views/dashboard/dashboard-content/bookings/bookings.component';
import { ChatsComponent } from './views/dashboard/dashboard-content/chats/chats.component';
import { SettingsComponent } from './views/dashboard/dashboard-content/settings/settings.component';
import { NotificationsComponent } from './views/dashboard/dashboard-content/notifications/notifications.component';
import { LoginBusinessComponent } from './views/login/login-business/login-business.component';
import { CongratulationsComponent } from './views/jobs/congratulations/congratulations.component';
import { SignInBusinessComponent } from './views/login/sign-in-business/sign-in-business.component';
import { SignInMumComponent } from './views/login/sign-in-mum/sign-in-mum.component';
import {JobsstaticComponent} from './views/static/jobsstatic/jobsstatic.component';
import {ServicesstaticComponent} from './views/static/services/servicesstatic.component';
import { ProfileviewComponent } from './views/dashboard/dashboard-content/profileview/profileview.component';
import { CardModalComponent } from './utils/card-modal/card-modal.component';
import { BankAccountModalComponent } from './utils/bank-account-modal/bank-account-modal.component';
import {environment} from '../environments/environment';
import { AcceptjobModalComponent } from './utils/acceptjob-modal/acceptjob-modal.component';
import { ChatComponent } from './views/dashboard/dashboard-content/chat/chat.component';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import {AngularFireModule} from 'angularfire2';

import {AngularFireDatabaseModule} from 'angularfire2/database';
import { GooglePlacesDirective } from './google-places.directive';
import { NotificationlistComponent } from './views/dashboard/dashboard-content/notificationlist/notificationlist.component';
import { ForgetpasswordComponent } from './views/login/forgetpassword/forgetpassword.component';
import { SetPasswordComponent } from './views/login/setpassword/setpassword.component';
import { MediaComponent } from './views/home/media/media.component';
import { WhyJugglrComponent } from './views/home/why-jugglr/why-jugglr.component';
import { FooterComponent } from './views/footer/footer.component';
import { FAQComponent } from './pages/faq/faq.component';
import { BusinessPremiumComponent } from './pages/business-premium/business-premium.component';
import { HelpComponent } from './pages/help/help.component';
import { TermsComponent } from './pages/terms/terms.component';
import { PrivacyComponent } from './pages/privacy/privacy.component';
import { InvoiceComponent } from './views/dashboard/dashboard-content/invoice/invoice.component';
import { EditprofileComponent } from './views/dashboard/dashboard-content/editprofile/editprofile.component';
import { ProfessionalswithskillComponent } from './views/dashboard/dashboard-content/professionalswithskill/professionalswithskill.component';
import { AnalyticsComponent } from './views/dashboard/dashboard-content/analytics/analytics.component';
import { NetworkComponent } from './views/dashboard/dashboard-content/network/network.component';
import { TestimonailComponent } from './views/testimonail/testimonail.component';
//import { MaddjobformComponent } from './views/maddjobform/maddjobform.component';
import { ArticlesComponent } from './pages/articles/articles.component';
import { UserprofilelistComponent } from './views/dashboard/dashboard-content/userprofilelist/userprofilelist.component';

// Configs
export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
    [
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider(environment.facebookid)
        //provider: new FacebookLoginProvider('1521527624820825')
      },
      // {
      //   id: GoogleLoginProvider.PROVIDER_ID,
      //   provider: new GoogleLoginProvider("Your-Google-Client-Id")
      // },
      // {
      //   id: LinkedinLoginProvider.PROVIDER_ID,
      //   provider: new LinkedinLoginProvider("1098828800522-m2ig6bieilc3tpqvmlcpdvrpvn86q4ks.apps.googleusercontent.com")
      // },
    ]
  );
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    HeadlineComponent,
    HeaderComponent,
    SponsorsComponent,
    ServicesComponent,
    JobsComponent,
    HomeComponent,
    LoginComponent,
    SocialloginComponent,
    AddjobformComponent,
    JoblistComponent,
    JobdetailComponent,
    ModalConfigComponent,
    DashboardMenuComponent,
    DashboardContentComponent,
    DashboardComponent,
    MarketplaceComponent,
    BookingsComponent,
    ChatsComponent,
    SettingsComponent,
    NotificationsComponent,
    LoginBusinessComponent,
    CongratulationsComponent,
    SignInBusinessComponent,
    SignInMumComponent,
    JobsstaticComponent,
    ServicesstaticComponent,
    ProfileviewComponent,
    CardModalComponent,
    BankAccountModalComponent,
    AcceptjobModalComponent,
    ChatComponent,
    GooglePlacesDirective,
    NotificationlistComponent,
    ForgetpasswordComponent,
    SetPasswordComponent,
    MediaComponent,
    WhyJugglrComponent,
    FooterComponent,
    FAQComponent,
    BusinessPremiumComponent,
    HelpComponent,
    TermsComponent,
    PrivacyComponent,
    InvoiceComponent,
    EditprofileComponent,
    ProfessionalswithskillComponent,
    AnalyticsComponent,
    NetworkComponent,
    TestimonailComponent,
    //MaddjobformComponent,
    ArticlesComponent,
    UserprofilelistComponent,
  ],
  imports: [
    GooglePlaceModule,
    BrowserModule,
    HttpClientModule,
    ROUTING,
    DlDateTimePickerDateModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    ChartModule,
    CalendarModule,
    BrowserAnimationsModule,
    InfiniteScrollModule
  ],
  providers: [
    AuthService,
    JugglrServices,
    S3UploadFileService,
    Job,
    Offer,
    AngularFireStorageModule,
    NgbRatingConfig,
    User,
    AcceptjobModalComponent,
    NgbModalConfig,
    BusinessPremiumComponent,
    NgbModal,
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [ModalConfigComponent,CardModalComponent,BankAccountModalComponent]
})

export class AppModule { }
