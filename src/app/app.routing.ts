import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core/src/metadata/ng_module';
import {LoginComponent} from '../app/views/login/login.component';
import {HomeComponent} from './views/home/home.component';
import {SocialloginComponent} from './views/sociallogin/sociallogin.component';
import {AddjobformComponent} from './views/addjobform/addjobform.component';
import {JoblistComponent} from './views/joblist/joblist.component';
import {JobdetailComponent} from './views/jobdetail/jobdetail.component';
import {MarketplaceComponent} from './views/dashboard/dashboard-content/marketplace/marketplace.component';
import {BookingsComponent} from './views/dashboard/dashboard-content/bookings/bookings.component';
import {ChatsComponent} from './views/dashboard/dashboard-content/chats/chats.component';
import {SettingsComponent} from './views/dashboard/dashboard-content/settings/settings.component';
import {NotificationsComponent} from './views/dashboard/dashboard-content/notifications/notifications.component';
import {DashboardContentComponent} from './views/dashboard/dashboard-content/dashboard-content.component';
import {DashboardComponent} from './views/dashboard/dashboard.component';
import { LoginBusinessComponent } from './views/login/login-business/login-business.component';
import { CongratulationsComponent } from './views/jobs/congratulations/congratulations.component';
import {SignInMumComponent} from './views/login/sign-in-mum/sign-in-mum.component';
import {SignInBusinessComponent} from './views/login/sign-in-business/sign-in-business.component';
import {JobsstaticComponent} from './views/static/jobsstatic/jobsstatic.component';
import {ServicesstaticComponent} from './views/static/services/servicesstatic.component';
import {ProfileviewComponent} from './views/dashboard/dashboard-content/profileview/profileview.component'
import { NotificationlistComponent } from './views/dashboard/dashboard-content/notificationlist/notificationlist.component';
import { ForgetpasswordComponent } from './views/login/forgetpassword/forgetpassword.component';
import { SetPasswordComponent } from './views/login/setpassword/setpassword.component';
import { FAQComponent } from './pages/faq/faq.component';
import { BusinessPremiumComponent } from './pages/business-premium/business-premium.component';
import { HelpComponent } from './pages/help/help.component';
import { TermsComponent } from './pages/terms/terms.component';
import { PrivacyComponent } from './pages/privacy/privacy.component';
import { InvoiceComponent } from './views/dashboard/dashboard-content/invoice/invoice.component';
import { EditprofileComponent } from './views/dashboard/dashboard-content/editprofile/editprofile.component';
import { AnalyticsComponent } from './views/dashboard/dashboard-content/analytics/analytics.component';
import { ProfessionalswithskillComponent } from './views/dashboard/dashboard-content/professionalswithskill/professionalswithskill.component';
import { NetworkComponent } from './views/dashboard/dashboard-content/network/network.component';
//import { MaddjobformComponent } from './views/maddjobform/maddjobform.component';
import { ArticlesComponent } from './pages/articles/articles.component';
import { UserprofilelistComponent } from './views/dashboard/dashboard-content/userprofilelist/userprofilelist.component';

export const AppRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'sociallogin', component: SocialloginComponent},
  {path: 'addjob', component: AddjobformComponent},
  //{path: 'muneebaddjob', component: MaddjobformComponent},

  {path: 'loginBusiness', component: LoginBusinessComponent},
  {path: 'congratulation', component: CongratulationsComponent},
  {path: 'listJob', component: JoblistComponent},
  {path: 'jobdetail', component: JobdetailComponent},
  {path: 'signinbusiness', component: SignInBusinessComponent},
  {path: 'signinmum', component: SignInMumComponent},
  {path: 'jobs', component: JobsstaticComponent},
  {path: 'services', component: ServicesstaticComponent},
  {path: 'forgotpassword', component: ForgetpasswordComponent},
  {path: 'setpassword', component: SetPasswordComponent},
  {path: 'help-and-support', component: HelpComponent},
  {path: 'FAQ', component: FAQComponent},
  {path: 'terms-and-conditions', component: TermsComponent},
  {path: 'privacy-policy', component: PrivacyComponent},
  {path: 'articles', component: ArticlesComponent},

  {path: 'premium', component: BusinessPremiumComponent},


  {path: 'dashbord', component: DashboardComponent, children:[
    {
      path: '',
      component: MarketplaceComponent,
      outlet: 'dashboardContent'
    },
      {
        path: 'profileview',
        component: ProfileviewComponent,
        outlet: 'dashboardContent'
      },{
        path: 'jobdetails',
        component: JobdetailComponent,
        outlet: 'dashboardContent'
      },{
        path: 'network',
        component: NetworkComponent,
        outlet: 'dashboardContent'
      },
    {
      path: 'marketplace',
      component: MarketplaceComponent,
      outlet: 'dashboardContent'
    },
    {
      path: 'editprofile',
      component: EditprofileComponent,
      outlet: 'dashboardContent'
    },
    {
      path: 'analytics',
      component: AnalyticsComponent,
      outlet: 'dashboardContent'
    },
    {
      path: 'pws',
      component: ProfessionalswithskillComponent,
      outlet: 'dashboardContent'
    },
    {
      path: 'invoice',
      component: InvoiceComponent,
      outlet: 'dashboardContent'
    },
    {
      path: 'bookings',
      component: BookingsComponent,
      outlet: 'dashboardContent'
    },
    {
      path: 'chats',
      component: ChatsComponent,
      outlet: 'dashboardContent'
    },
    {
      path: 'notifications',
      component: NotificationsComponent,
      outlet: 'dashboardContent'
    },
    {
      path: 'allnotifications',
      component: NotificationlistComponent,
      outlet: 'dashboardContent'
    },
    {
      path: 'settings',
      component: SettingsComponent,
      outlet: 'dashboardContent'
    },
    {
      path: 'userprofilelist',
      component: UserprofilelistComponent,
      outlet: 'dashboardContent'
    },
  ]},
  { path: "**",redirectTo:""}

];

export const ROUTING: ModuleWithProviders = RouterModule.forRoot(AppRoutes);
