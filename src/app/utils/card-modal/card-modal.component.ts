import {
  Component,
  AfterViewInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  ChangeDetectorRef
} from '@angular/core';

import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import { JugglrServices } from '../../Services/JugglrServices';

declare var stripe: any;
declare var elements: any;

@Component({
  selector: 'app-card-model',
  templateUrl: './card-modal.component.html',
  styleUrls: ['./card-modal.component.css']
})
export class CardModalComponent implements AfterViewInit, OnDestroy{
  @ViewChild('cardNumber') cardNumber: ElementRef;
  @ViewChild('cardExpiry') cardExpiry: ElementRef;
  @ViewChild('cardCvc') cardCvc: ElementRef;

  card_number: any;
  card_expiry: any;
  card_cvc: any;
  cardHandler = this.onChange.bind(this);
  error: string;

  constructor(config: NgbModalConfig, private modalService: NgbModal,private jservices: JugglrServices,private router: Router,private cd: ChangeDetectorRef) {
  }

  open(content) {
    this.modalService.open(content, { centered: true });
  }

  ngAfterViewInit() {
    //Card Account
    this.card_number = elements.create('cardNumber');
    this.card_number.mount(this.cardNumber.nativeElement);
    this.card_number.addEventListener('change', this.cardHandler);

    this.card_expiry = elements.create('cardExpiry');
    this.card_expiry.mount(this.cardExpiry.nativeElement);
    this.card_expiry.addEventListener('change', this.cardHandler);

    this.card_cvc = elements.create('cardCvc');
    this.card_cvc.mount(this.cardCvc.nativeElement);
    this.card_cvc.addEventListener('change', this.cardHandler);
  }

  ngOnDestroy() {
    this.card_number.removeEventListener('change', this.cardHandler);
    this.card_number.destroy();

    this.card_expiry.removeEventListener('change', this.cardHandler);
    this.card_expiry.destroy();

    this.card_cvc.removeEventListener('change', this.cardHandler);
    this.card_cvc.destroy();
  }

  onChange({error}) {
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }

  async onSubmit(form: NgForm) {
    const {token, error} = await stripe.createToken(this.card_number, this.card_expiry, this.card_cvc);

    if (error) {
      console.log('Something is wrong:', error);
    } else {
      console.log('Success!', token);
      this.jservices.updateccdetails(token['id'],'','','')
      .subscribe(data => {
        if (data['status']==1){
          var profile = JSON.parse(localStorage.getItem('currentUser'));
          profile['stripe_cust_id']=data['data']['stripe_cust_id'];
          localStorage.setItem('currentUser',JSON.stringify(profile));
          this.modalService.dismissAll();
        }
      });
    }
  }
  d(s){
    this.modalService.dismissAll();
  }
}
