import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../../../models/user';
import { JugglrServices } from '../../../Services/JugglrServices';
import {ActivatedRoute,Router} from '@angular/router';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { GooglePlacesDirective } from 'src/app/google-places.directive';


@Component({
  selector: 'app-sign-in-business',
  templateUrl: './sign-in-business.component.html',
  styleUrls: ['./sign-in-business.component.css']
})
export class SignInBusinessComponent implements OnInit {
  loading=false;
  login: boolean = true;
  message: string;
  options = {
    types: [],
    componentRestrictions: { country: 'AU' }
    }
  from="";
  constructor(private user: User, private jservices: JugglrServices, private router:Router,private route: ActivatedRoute, ) {
   
    
   }

  ngOnInit() {
    this.route
    .queryParams
    .subscribe(params => {
      // Defaults to 0 if no query param provided.
      this.from = params['from'].toString();
    });
  }

  @ViewChild("placesRef") placesRef : GooglePlacesDirective;
    
        public handleAddressChange(address: Address) {
          this.user.address= address['formatted_address'];
        console.log(address);
    }

  goSignUp() {
    this.login=!this.login;
  }

  signin(){
    this.loading=true;
    console.log("Business User: " + this.user);
    this.jservices.businessLogin(this.user)
      .subscribe(data => {
        console.log(data);
        if (data['status'] == 1) {//user signedin
          localStorage.setItem('currentUser', JSON.stringify(data['data']['result']));
          this.loading=false;
          if(this.from=="premium")
          this.router.navigate(["/premium"]);
          else
          this.router.navigate(["/dashbord"]);
        }
        else{
          this.message = data['message'];
          this.loading=false;
        }
      });
  }

  registration(){
    console.log(this.user);
    this.jservices.businessregistration(this.user)
      .subscribe(data => {
        console.log(data);
        if (data['status'] == 1) {//user signedin
          localStorage.setItem('currentUser', JSON.stringify(data['data']['result']));
          if(this.from=="premium")
          this.router.navigate(["/premium"]);
          else
          this.router.navigate(["/dashbord"]);        }
        else{
          this.message = data['message'];
        }
        this.loading=false;
      });
  }

 
  showlogin(){
    this.login = true;
  }

  startregistred(){
    this.user
    this.loading=true;
    this.jservices.getPlaceDetail(this.user.address)
      .subscribe(data => {
        var res=data;
        if(res["status"]=='ZERO_RESULTS' || res['error_message']){
            this.message='please enter a valide address';
            this.loading=false;
            return false;
        }
        else if(data['results'].length>0){
          var componentslist=(data['results'][0]['address_components'])
          for (var i=0;i< componentslist.length;i++){
              if(componentslist[i].types[0]=='postal_code')
              this.user['postcode']=componentslist[i].short_name
      
              if(componentslist[i].types[0]=='locality')
              this.user['suburb']=componentslist[i].short_name
      
              if(componentslist[i].types[0]=='administrative_area_level_1')
              this.user['state']=componentslist[i].short_name
      
              if(componentslist[i].types[0]=='country')
              this.user['country']=componentslist[i].short_name
      
              if(componentslist[i].types[0]=='administrative_area_level_2')
              this.user['city']=componentslist[i].short_name
          }
          this.user['latitude']=data['results'][0].geometry["location"]["lat"];
          this.user['longitude']=data['results'][0].geometry["location"]["lng"];
          console.log('after google location')
          console.log(this.user);
  
            //registration
            this.registration();
  
        }
        else{
          this.message='Something went wrong!';
          this.loading=false;
        }

      });
  }

}
