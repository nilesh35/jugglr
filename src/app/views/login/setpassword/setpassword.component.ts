import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { JugglrServices } from '../../../Services/JugglrServices';

@Component({
  selector: 'app-setpassword',
  templateUrl: './setpassword.component.html',
  styleUrls: ['./setpassword.component.css']
})
export class SetPasswordComponent implements OnInit {
  password:string;
  confirmPassword:string;
  loading=false;
  changedone=false;
  message="";
  token = "";
  constructor(private modal:NgbModal,private jservices: JugglrServices, private router:Router,private route: ActivatedRoute) { }

  ngOnInit() {
   this.password="";
   this.confirmPassword="";
   this.setToken();
  }

  setToken() {
   this.route
   .queryParams
   .subscribe(params => {
     // Defaults to 0 if no query param provided.
     this.token = params['token'].toString();
   });
  }
  opendsignup(){
    this.router.navigate(["signinbusiness"]);
  }
  setpassword(){
    this.message="";
    this.loading=true;
    this.jservices.setpassword(this.password,this.confirmPassword,this.token)
      .subscribe(data => {
        console.log(data);
        if (data['status'] == 1) {//user signedin
          this.loading=false;
          this.password="";
          this.confirmPassword="";
          this.changedone=true;
        }
        else{
          this.message = data['message'];
          this.loading=false;
        }
      });
  }
}
