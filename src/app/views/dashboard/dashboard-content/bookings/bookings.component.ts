import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from "@angular/router";
import { JugglrServices } from '../../../../Services/JugglrServices';
import { NgbModal, NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { PaymentServiceService } from 'src/app/payment-service.service';
import { environment } from 'src/environments/environment';
import { User } from 'src/app/models/user';
import { Observable } from 'rxjs';
import { debounceTime, switchMap, tap } from 'rxjs/operators';
@Component({
   selector: 'app-bookings',
   templateUrl: './bookings.component.html',
   styleUrls: ['./bookings.component.css']
})
export class BookingsComponent implements OnInit {

   constructor(private currentuser: User, private paymentSvc: PaymentServiceService, private router: Router, private jservices: JugglrServices, private modalService: NgbModal) { }
   socialjobs: any = [];
   marketplacejobs: any = [];
   selectedjobforRating: any;
   transactiondtail: any;
   showmessage = false;
   alertmesssage = "";
   handler: any;
   amount = 1000;
   pagegenumber = 1;
   searchVal = "";
   searchbox: any;
   blnSearched:boolean=false;
   totalpages = { type1: 0, type0: 0 };
   ngOnInit() {
      //this.marketplacejobs.push({datetime:Date(), tab:'123',userwithname:'hello', subservices:'123', amount:1000, type:'need', is_completed:false, is_accepted:false, is_rated:true});
      this.handler = StripeCheckout.configure({
         key: environment.stripeKey,
         image: '/assets/iconblue.png',
         locale: 'auto',
         token: token => {
            console.log(this.handler.stripeEmail);
            this.paymentSvc.processcard(token, this.amount)
            console.log(token)
            this.jservices.updateccdetails(token['id'], '', '', '')
               .subscribe(data => {
                  if (data['status'] == 1) {
                     var profile = JSON.parse(localStorage.getItem('currentUser'));
                     profile['stripe_cust_id'] = data['data']['stripe_cust_id'];
                     localStorage.setItem('currentUser', JSON.stringify(profile));
                     const profilex = JSON.parse(localStorage.getItem('currentUser'));
                     const userid = profilex['id'].toString();
                     const usertoken = profilex['user_token'];
                     const devicetype = profilex['device_type'];
                     this.currentuser.myconstructor(profilex);
                  }
               });

         },
         stripeEmail: email => { console.log(email) }

      });
      console.log("this.pagegenumber : " + this.pagegenumber + " this.totalpages.type1 : " + this.totalpages.type1 + " this.totalpages.type0 : " + this.totalpages.type0 + " this.searchVal : " + this.searchVal);
      this.getBookingData();
   }

   getBookingData() {
      if(this.searchVal == undefined) this.searchVal= "";
      var search_id = this.searchVal.substr(this.searchVal.lastIndexOf("-") + 1);
      this.jservices.mytodo('1', this.pagegenumber + "",search_id).subscribe(data => {
         this.totalpages.type1 = data['data']['totalpages'];
         if (data["data"].jobs.length) {
            for (var i = 0; i < data["data"].jobs.length; i++) {
               this.marketplacejobs.push(data["data"].jobs[i])
            }
         }
         //this.offerjob=data.data.jobs
         console.log('marketplacejobs')
         console.log(this.marketplacejobs)
      });

      this.jservices.mytodo('0', this.pagegenumber + "",search_id).subscribe(data => {
         this.totalpages.type0 = data['data']['totalpages'];
         if (data["data"].jobs.length) {
            for (var i = 0; i < data["data"].jobs.length; i++) {
               this.marketplacejobs.push(data["data"].jobs[i])
            }
         } 
         //this.needjobs=data.data.jobs
         console.log(this.marketplacejobs)
      });
   }

   showalert(message) {
      this.alertmesssage = message;
      this.showmessage = true;
      setTimeout(() => {
         this.showmessage = false;
         this.alertmesssage = "";
      }, 4000);
   }

   onPageScroll() {
      console.log("Page Scrolled");
      this.getnext();
   }
   getnext() {
      if(this.searchVal == undefined) this.searchVal= "";3
      var search_id = this.searchVal.substr(this.searchVal.lastIndexOf("-") + 1);
      console.log("this.pagegenumber : " + this.pagegenumber + " this.totalpages.type1 : " + this.totalpages.type1 + " this.totalpages.type0 : " + this.totalpages.type0)
      this.pagegenumber++;
      console.log("this.pagegenumber : " + this.pagegenumber + " this.totalpages.type1 : " + this.totalpages.type1 + " this.totalpages.type0 : " + this.totalpages.type0)
      if (this.pagegenumber <= this.totalpages.type1) {
         this.jservices.mytodo('1', this.pagegenumber + "", search_id).subscribe(data => {
            this.totalpages.type1 = data['data']['totalpages'];
            if (data["data"].jobs.length) {
               for (var i = 0; i < data["data"].jobs.length; i++) {
                  this.marketplacejobs.push(data["data"].jobs[i])
               }
            }
            //this.offerjob=data.data.jobs
            console.log('marketplacejobs')
            console.log(this.marketplacejobs)
         });
      }
      if (this.pagegenumber <= this.totalpages.type0) {
         this.jservices.mytodo('0', this.pagegenumber + "", search_id).subscribe(data => {
            this.totalpages.type0 = data['data']['totalpages'];
            if (data["data"].jobs.length) {
               for (var i = 0; i < data["data"].jobs.length; i++) {
                  this.marketplacejobs.push(data["data"].jobs[i])
               }
            }
            //this.needjobs=data.data.jobs
            console.log(this.marketplacejobs)
         });
      }
   }

   refrech() {
      this.pagegenumber = 1;
      this.socialjobs = [];
      this.marketplacejobs = [];
      this.selectedjobforRating = null;
      this.transactiondtail = null;
      
      if(this.searchVal == undefined) this.searchVal= "";
      var search_id = this.searchVal.substr(this.searchVal.lastIndexOf("-") + 1);
      this.jservices.mytodo('1', this.pagegenumber + '', search_id).subscribe(data => {
         if (data["data"].jobs.length)
            for (var i = 0; i < data["data"].jobs.length; i++) {
               this.marketplacejobs.push(data["data"].jobs[i])
            }
         //this.offerjob=data.data.jobs
         console.log('marketplacejobs')
         console.log(this.marketplacejobs)
      });

      this.jservices.mytodo('0', this.pagegenumber + '', search_id).subscribe(data => {
         if (data["data"].jobs.length)
            for (var i = 0; i < data["data"].jobs.length; i++) {
               this.marketplacejobs.push(data["data"].jobs[i])
            }
         //this.needjobs=data.data.jobs
         console.log(this.marketplacejobs)
      });
   }
   postsetoffercomplete(job: any) {
      console.log(job)
      this.jservices.confirmcomplete(job.id).subscribe(data => {
         console.log(data)
         this.showalert("Thank you for confirming gig completion.")
         this.refrech()
      })
   }
   open(content, job) {
      this.selectedjobforRating = job;
      this.modalService.open(content, { centered: true }).result.then((result) => {
      }, (reason) => {
      });
   }
   reviewoffer(content2, job) {
      this.jservices.gettransactiondetail(job.id).subscribe(data => {
         if (data["status"]) {
            console.log(data["data"]);
            this.transactiondtail = data["data"];
            this.modalService.open(content2, { centered: true }).result.then((result) => {
            }, (reason) => {
            });
         }

      })

   }
   goUserDetail(userId) {
      this.router.navigate(['/dashbord', { outlets: { dashboardContent: ['profileview'] } }], { queryParams: { id: userId } });
   }

   addtransactionfeedback(s: string) {
      console.log(this.selectedjobforRating)
      this.jservices.addtransactionfeedback(this.selectedjobforRating.id, s).subscribe(data => {
         console.log(data)
         this.showalert("Thank you for rating " + this.selectedjobforRating.userwithname + ".")
         this.modalService.dismissAll();
         this.selectedjobforRating = null;

         this.refrech()
      })
   }
   accepttransaction(s: string) {
      console.log(this.transactiondtail)
      this.amount = this.transactiondtail.amount * 100
      console.log(this.currentuser)
      if (this.currentuser.stripe_cust_id == '') {
         this.handlePayment()
      } else {
         this.jservices.accepttransactiondetail(this.transactiondtail.id, s).subscribe(data => {
            console.log(data)
            this.showalert("You agree to pay " + this.transactiondtail.user_name + " $" + this.transactiondtail.amount + " and your card will be charged $" + this.transactiondtail.amount + " after gig completion.")
            this.modalService.dismissAll();
            this.transactiondtail = null;
            this.refrech()
         })
      }

   }


   handlePayment() {
      this.handler.open({
         name: 'Jugglr',
         excerpt: this.transactiondtail.customservice,
         description: this.transactiondtail.customservice,
         amount: this.transactiondtail.amount * 100,


      });
   }
   @HostListener('window:popstate')
   onPopstate() {
      this.handler.close()
   }



   someMethod() {

      //this.subscribehttp().subscribe(data => {});
   }

   reviewdetail(content3, job) {
      let notifications = [];
      this.jservices.currentnotificationdata.subscribe(notificationdata => {      
        if (notificationdata['totalRecords'] > 0){
          notifications = [];
          notificationdata['notification'].forEach(element => {
            notifications.push(element)
          });
        }
      });

      notifications.forEach((element,index) => {
         if(element['data']['redirect'] == "MYTODO" && element['data']['params']['offer_data']['id'] == job['id'] && element['data']['params']['offer_data']['user_with_id'] == job['user_with_id']) {
            this.jservices.updatenotificationreadflag(element['id']+"").subscribe(data=>{
               if (data['status'] != null){
                  let notificationfinaldatalist = {};
                  notifications.splice(index,1);
                  notificationfinaldatalist['notification'] = notifications;
                  notificationfinaldatalist['totalRecords'] = notifications.length;            
                  this.jservices.updatenotificationreaddata(notificationfinaldatalist);            
               }
            });
         }
       });

      this.jservices.gettransactiondetail(job.id).subscribe(data => {
         if (data["status"]) {
            console.log(data["data"]);
            this.transactiondtail = data["data"];
            this.modalService.open(content3, { centered: true }).result.then((result) => {
            }, (reason) => {
            });
         }

      })

   }
   closeallpopup() {
      this.modalService.dismissAll();
   }

   addgig() {
      this.router.navigate(['/addjob']);

   }
   hiretalent() {
      this.router.navigate(['/dashbord']);

   }

   search = (text$: Observable<string>) =>
      text$.pipe(
         debounceTime(200),
         tap(() => {
            this.searchVal = (<HTMLInputElement>window.document.getElementsByName('textsearch')[0]).value;
            //if(this.searchbox != undefined && this.searchVal == this.searchbox.search_index)
            if(this.searchbox != undefined && this.searchVal == this.searchbox.searchVal) { 
               this.searchVal = "";
            }
            console.log("search 1 tap ::" , this.searchbox)
         }),
         switchMap(term => term.length > 2 ? 
            this.jservices.autosuggestionsearch(term,'1') : []
            )
      )

   formatter = (x: { search_index: string }) => x.search_index + "";
 
   inputformatter = (x: { search_index: string, search_value: string }) => {
      //x.search_value
      return x.search_index ? x.search_index + "" : "";
   };
   
   selectedItem(item){
      if(item) {
         //console.log(item.item);
         //this.searchVal = item.item.search_index;
         this.searchVal = item.item.search_value;

         this.searchbox = item.item;
         this.changesearch()
      }
    }
    clearSearch() {
      this.searchVal = "";
      this.searchbox = undefined;
      this.changesearch();
   }
    changesearch() {
      //if(this.searchbox == undefined || this.searchVal != this.searchbox.search_index)
      if(this.searchbox == undefined || this.searchVal != this.searchbox.search_value) 
      {
         if(this.searchVal != "") {
            return;
         }
      }
      this.marketplacejobs = [];
      this.pagegenumber = 1;
      if(this.searchVal != "") {
         this.blnSearched = true;   
      }
      else {
         this.blnSearched = false;   
      }
      this.getBookingData();
   }
}
