import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from "@angular/router";
import { JugglrServices } from '../../../../Services/JugglrServices';
import { NgbModal, NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { PaymentServiceService } from 'src/app/payment-service.service';
import { environment } from 'src/environments/environment';
import { User } from 'src/app/models/user';
import * as jsPDF from 'jspdf'
import * as html2pdf from 'html2pdf.js'
import { Observable } from 'rxjs';
import { debounceTime, switchMap, tap } from 'rxjs/operators';


@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent implements OnInit {

  constructor(private currentuser: User, private paymentSvc: PaymentServiceService, private router: Router, private jservices: JugglrServices, private modalService: NgbModal) { }
  socialjobs: any = [];
  marketplacejobs: any = [];
  selectedjobforRating: any;
  transactiondtail: any;
  showmessage = false;
  alertmesssage = "";
  handler: any;
  amount = 1000;
  ABN: string = "";
  detailjobselected: any = [];
  userwithselected: any;
  searchVal = "";
   searchbox: any;
   blnSearched:boolean=false;
  ngOnInit() {
    const profile = JSON.parse(localStorage.getItem('currentUser'));
   
    this.currentuser.myconstructor(profile);
    this.ABN = localStorage.getItem("ABN")
    this.getInvoiceData();

    
  }
  getInvoiceData() {
   this.jservices.mycompletedtodo('1', this.searchVal).subscribe(data => {
      if (data["data"].jobs.length)
        for (var i = 0; i < data["data"].jobs.length; i++) {
          if (data["data"].jobs[i]['tab'] == 1){
            this.marketplacejobs.push(data["data"].jobs[i])
          }
        }
      //this.offerjob=data.data.jobs
      console.log('marketplacejobs')
      console.log(this.marketplacejobs)
    });

    this.jservices.mycompletedtodo('0',this.searchVal).subscribe(data => {
      if (data["data"].jobs.length)
        for (var i = 0; i < data["data"].jobs.length; i++) {
          if (data["data"].jobs[i]['tab'] == 1){
            this.marketplacejobs.push(data["data"].jobs[i])
          }
        }

      //this.needjobs=data.data.jobs
      console.log(this.marketplacejobs)
    });
  }

  showalert(message) {
    this.alertmesssage = message;
    this.showmessage = true;
    setTimeout(() => {
      this.showmessage = false;
      this.alertmesssage = "";
    }, 4000);
  }
  refrech() {
    this.socialjobs = [];
    this.marketplacejobs = [];
    this.selectedjobforRating = null;
    this.transactiondtail = null;
    this.jservices.mytodo('1','0','').subscribe(data => {
      if (data["data"].jobs.length)
        for (var i = 0; i < data["data"].jobs.length; i++) {
          this.marketplacejobs.push(data["data"].jobs[i])
        }
      //this.offerjob=data.data.jobs
      console.log('marketplacejobs')
      console.log(this.marketplacejobs)
    });

    this.jservices.mytodo('0','0','').subscribe(data => {
      if (data["data"].jobs.length)
        for (var i = 0; i < data["data"].jobs.length; i++) {
          this.marketplacejobs.push(data["data"].jobs[i])
        }
      //this.needjobs=data.data.jobs
      console.log(this.marketplacejobs)
    });
  }
  postsetoffercomplete(job: any) {
    console.log(job)
    this.jservices.confirmcomplete(job.id).subscribe(data => {
      console.log(data)
      this.showalert("Thank you for confirming gig completion.")
      this.refrech()
    })
  }
  open(content, job) {
    this.selectedjobforRating = job;
    this.modalService.open(content, { centered: true }).result.then((result) => {
    }, (reason) => {
    });
  }
  reviewoffer(content2, job) {
    this.jservices.gettransactiondetail(job.id).subscribe(data => {
      if (data["status"]) {
        console.log(data["data"]);
        this.transactiondtail = data["data"];
        this.modalService.open(content2, { centered: true }).result.then((result) => {
        }, (reason) => {
        });
      }

    })

  }
  goUserDetail(userId) {
    this.router.navigate(['/dashbord', { outlets: { dashboardContent: ['profileview'] } }], { queryParams: { id: userId } });
  }



  reviewdetail(content3, job) {
    this.jservices.gettransactiondetail(job.id).subscribe(data => {
      if (data["status"]) {
        console.log(data["data"]);
        this.transactiondtail = data["data"];
        this.modalService.open(content3, { centered: true }).result.then((result) => {
        }, (reason) => {
        });
      }

    })

  }
  closeallpopup() {
    this.modalService.dismissAll();
  }
  selectjob:any;
  invoiceready=false;
  contractready=false;
  downloadinvoice(content4, job) {
    console.log(this.ABN)
    this.selectjob=job
    if (this.ABN == null)
      this.modalService.open(content4, { centered: true }).result.then((result) => {
      }, (reason) => {
      });
    else {
      console.log(job)
      //console.log(window.document.getElementsByClassName('c11')[0].innerHTML)
      this.jservices.gettransactiondetail(job.id).subscribe(data => {
        if (data["status"]) {
          console.log(this.selectjob)
          this.showalert("Loading invoice... please wait.")
          this.detailjobselected = data["data"];
          this.jservices.getUserfullDetail(job.user_with_id, this.currentuser).subscribe(dat => {
            if (dat["status"]) {
              this.userwithselected = dat["data"][0];
              this.invoiceready=true;
              console.log(this.userwithselected)
              window.setTimeout(function(){
                var element = window.document.getElementsByClassName('invoice')[0].innerHTML;
                var opt = {
                  margin: 1,
                  filename: 'invoice#.pdf',
                  image: { type: 'jpeg', quality: 1 },
                  html2canvas: { scale: 2 },
                  pagebreak: { mode: ['css', 'legacy'] },
                  jsPDF: { unit: 'in', format: 'a4', orientation: 'portrait' }
                };
                var worker = html2pdf().set(opt).from(element).save();
                this.invoiceready=false;

              },1000)
              
            }
            else 
            this.showalert("Something went wrong")
          })
        }
        else 
        this.showalert("Something went wrong")
      })



    }
  }

  downloadcontract(content4, job) {
    console.log(this.ABN)
    this.selectjob=job
    if (this.ABN == null)
      this.modalService.open(content4, { centered: true }).result.then((result) => {
      }, (reason) => {
      });
    else {
      console.log(job)
      //console.log(window.document.getElementsByClassName('c11')[0].innerHTML)
      this.jservices.gettransactiondetail(job.id).subscribe(data => {
        if (data["status"]) {
          console.log(this.currentuser)
          this.showalert("Loading contract... please wait.")
          this.detailjobselected = data["data"];
          this.jservices.getUserfullDetail(job.user_with_id, this.currentuser).subscribe(dat => {
            if (dat["status"]) {
              this.userwithselected = dat["data"][0];
              this.contractready=true;
              console.log(this.userwithselected)
              window.setTimeout(()=>{
                var element = window.document.getElementsByClassName('contract')[0].innerHTML;
                var opt = {
                  margin: 1,
                  filename: 'contract#.pdf',
                  image: { type: 'jpeg', quality: 1 },
                  html2canvas: { scale: 2 },
                  pagebreak: { mode: ['css', 'legacy'] },
                  jsPDF: { unit: 'in', format: 'a4', orientation: 'portrait' }
                };
                var worker = html2pdf().set(opt).from(element).save();
                this.contractready=false;

              },1000)
              
            }
            else 
            this.showalert("Something went wrong")
          })
        }
        else 
        this.showalert("Something went wrong")
      })



    }
  }
  SAVEABN() {
    localStorage.setItem("ABN", this.ABN)
    this.showalert("Thank you for providing your ABN you can now download your document.")
    this.modalService.dismissAll();
  }
  addgig(){
    this.router.navigate(['/addjob']);

  }
  hiretalent(){
    this.router.navigate(['/dashbord']);

  }
  search = (text$: Observable<string>) =>
   text$.pipe(
      debounceTime(200),
      tap(() => {
         //console.log(this.searchbox)
         //this.searchVal = this.searchbox;

         this.searchVal = (<HTMLInputElement>window.document.getElementsByName('textsearch')[0]).value;
         if(this.searchbox != undefined && this.searchVal == this.searchbox.search_index) { 
            this.searchVal = "";
         }
         console.log("search 1 tap ::" , this.searchbox);
      }),
      switchMap(term => term.length > 2 ?
         this.jservices.autosuggestionsearch(term,'1') : []
      )
   )

   formatter = (x: { search_index: string }) => x.search_index + "";

   inputformatter = (x: { search_index: string, search_value: string }) => {
      //x.search_value
      console.log(this.searchbox)
      return x.search_index ? x.search_index + "" : "";
   };

   selectedItem(item) {
      //this.clickedItem=item.item;
      if (item) {
         console.log("searchVal", item.item.search_index);
         this.searchVal = item.item.search_index;
         this.searchbox = item.item;
         this.changesearch();   
      }
   }
   clearSearch() {
      this.searchVal = "";
      this.searchbox = undefined;
      this.changesearch();
   }
   changesearch() {
      if(this.searchbox == undefined || this.searchVal != this.searchbox.search_index) {
         if(this.searchVal != "") {
            return;
         }
      }
      this.marketplacejobs = [];
      if(this.searchVal != "") {
         this.blnSearched = true;   
      }
      else {
         this.blnSearched = false;   
      }
      this.getInvoiceData();
   }
}
