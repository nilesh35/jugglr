import { Component, OnInit, ElementRef } from '@angular/core';
//import {firebase} from "firebase";
import * as moment from 'moment';

import {User} from '../../../../models/user';
import {AngularFireDatabase,AngularFireList} from 'angularfire2/database';
import { JugglrServices } from 'src/app/Services/JugglrServices';
import { S3UploadFileService } from 'src/app/Services/S3UploadService';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-chats',
  templateUrl: './chats.component.html',
  styleUrls: ['./chats.component.css']
})
export class ChatsComponent implements OnInit {
  chatlist:Map<string,Messagelist>=new Map();

  chatprofiles=[];
  messagetype="";
  chathistory:any;
  selectedFiles: FileList;
  selectedFilesError:string = ''
  sendingChatMessage:boolean = false;

  constructor(private router: Router,private route: ActivatedRoute,private jservices: JugglrServices,private currentuser:User,private db:AngularFireDatabase, private s3uploadService: S3UploadFileService) { }
   selectedid=0;

   
  ngOnInit() {
    const profile = JSON.parse(localStorage.getItem('currentUser'));
    //console.log(profile);
    this.currentuser.myconstructor(profile);
    
    var ref=(this.db.list('/users/'+this.currentuser.Id+'/message'));  // "[DEFAULT]"
    ref.snapshotChanges().subscribe(item=>{
      //console.log(item)
      this.chatlist=new Map();
      this.chatprofiles=[];
      this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        var id = params['id'].toString();
        var nname = params['name'].toString();
        var pic= params['pic'].toString();
        var mm=new Messagelist();
        mm.id=parseInt(id);
        mm.name=nname;
        mm.pic=pic;
        mm.messages=new Array();
        if(!this.chatlist.has(""+id))
        this.chatlist.set(""+id,mm);
        this.selectedid=id;
      });

      item.forEach(element=>{
        var m=element.payload.toJSON() as Message;
        console.log("m = ",m);
        if(m.mReceiverId+""!=this.currentuser.Id+""){
          if(!this.chatlist.has(m.mReceiverId+"")){
            let messagelist= new Messagelist();
            messagelist.id=m.mReceiverId
            messagelist.name=m.receiverName
            messagelist.pic=m.receiverAvtarURL
            messagelist.totreceived=0;
            messagelist.lasttime=m.timeStamp;

            //this.chatprofiles.push(messagelist)
            messagelist.messages=new Array();
            messagelist.messages.push(m);
            this.chatlist.set(m.mReceiverId+"",messagelist);

          }
          else{
            this.chatlist.get(m.mReceiverId+"").pic=m.receiverAvtarURL;
            this.chatlist.get(m.mReceiverId+"").messages.push(m);
            this.chatlist.get(m.mReceiverId+"").lasttime=m.timeStamp;

          }
        }
        else if(m.mSenderUserId+""!=''+this.currentuser.Id){
          if(!this.chatlist.has(m.mSenderUserId+"")){
            let messagelist= new Messagelist();
            messagelist.id=m.mSenderUserId
            messagelist.name=m.senderName
            messagelist.pic=m.senderAvtarUrl
            messagelist.totreceived=1;
            messagelist.lasttime=m.timeStamp;

            //this.chatprofiles.push(messagelist)
            messagelist.messages=new Array();
            messagelist.messages.push(m);
            this.chatlist.set(m.mSenderUserId+"",messagelist);
          }
          else{
            this.chatlist.get(m.mSenderUserId+"").pic=m.senderAvtarUrl;
            this.chatlist.get(m.mSenderUserId+"").messages.push(m)
            this.chatlist.get(m.mSenderUserId+"").totreceived++
            this.chatlist.get(m.mSenderUserId+"").lasttime=m.timeStamp;
          }
        }
      })
      this.checkfornew();
    })
    
    this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        var id = params['id'].toString();
        var nname = params['name'].toString();
        var pic= params['pic'].toString();
        var mm=new Messagelist();
        mm.id=parseInt(id);
        mm.name=nname;
        mm.pic=pic;
        this.selectedid=id;
        mm.messages=new Array();
        if(!this.chatlist.has(""+id))
        this.chatlist.set(""+id,mm);
      });
      
  }
  select(id){
    this.selectedid=id;
    let totalunreadmessage = this.chatlist.get(''+id).totreceived;
    this.chathistory[id+""]=this.chatlist.get(''+id).totreceived;
    localStorage.setItem('chathistory',JSON.stringify(this.chathistory))
    this.checkfornew();

    let totalmessagereaded = 0;
    let notifications = [];
    this.jservices.currentnotificationdata.subscribe(notificationdata => {      
      if (notificationdata['totalRecords'] > 0){
        notifications = [];
        notificationdata['notification'].forEach(element => {
          notifications.push(element)
        });
      }
    });

    notifications.forEach((element,index) => {
      if(element['data']['redirect'] == "CHAT" && totalmessagereaded < totalunreadmessage) {
        this.jservices.updatenotificationreadflag(element['id']+"").subscribe(data=>{
          if (data['status'] != null){
            let notificationfinaldatalist = {};
            notifications.splice(index,1);
            notificationfinaldatalist['notification'] = notifications;
            notificationfinaldatalist['totalRecords'] = notifications.length;            
            this.jservices.updatenotificationreaddata(notificationfinaldatalist);            
          }
        });
        totalmessagereaded++;
      }
    });

    setTimeout(()=>{document.getElementsByClassName('msg_history')[0].scrollBy(0,document.getElementsByClassName('msg_history')[0].scrollHeight)},200);
  }
  checkfornew(){
    this.chatprofiles=[];
    this.chathistory=localStorage.getItem('chathistory')?JSON.parse(localStorage.getItem('chathistory')):JSON.parse('{}');
    this.chatlist.forEach(element => {
      if(this.chathistory[(""+element.id)]!=null){
        element.nbnew=element.totreceived-this.chathistory[""+element.id]
      }
      else{
        this.chathistory[(""+element.id)]=0;
      }
      localStorage.setItem('chathistory',JSON.stringify(this.chathistory))
      this.chatprofiles.push(element);
    });

    this.chatprofiles.sort((a: any, b: any) => {
      return this.compare(a,b)
    });
  }
  compare(a:Messagelist, b:Messagelist) {
    if (a.lasttime>b.lasttime) {
      return -1;
    }
    if (a.lasttime<b.lasttime) {
      return 1;
    }
    // a must be equal to b
    return 0;
  }
  timeSince(date) {
    return moment(date).fromNow();
  }
  goBack(){
    this.selectedid=0;
  }
  sendmessage(){
   this.selectedFilesError = '';
   this.sendingChatMessage = true;
   //var messagetypeText = this.parseChatMessage(this.messagetype);
   var mMessage=new Message()
   mMessage.imageThumbnail="";
   mMessage.mReceiverId=this.selectedid;
   mMessage.mSenderUserId=parseInt(this.currentuser.Id);
   mMessage.message=this.messagetype;
   mMessage.messageType="0";
   mMessage.receiverAvtarURL=this.chatlist.get(''+this.selectedid).pic;
   mMessage.receiverName=this.chatlist.get(''+this.selectedid).name;
   mMessage.senderName=this.currentuser.firstname;
   mMessage.senderAvtarUrl=this.currentuser.profile_image_thumb;
   mMessage.timeStamp=Date.now();
   
   mMessage.attachmentServerImagePath="";
   mMessage.attachmentSize="";
   
   if(this.selectedFiles) {
      var self = this;
      self.s3uploadService.uploadfile(self.selectedFiles, function (res) {
         mMessage.attachmentServerImagePath=res.key;
         mMessage.attachmentSize= self.selectedFiles["size"];
         self.chatlist.get(""+self.selectedid).messages.push(mMessage);
         var ref=(self.db.list('/users/'+self.currentuser.Id+'/message').push(mMessage));
         var refx=(self.db.list('/users/'+self.selectedid+'/message').push(mMessage));
         self.jservices.sendmessagenotification(self.selectedid+"").subscribe(data=>{
         console.log(data)
         })
         // setTimeout(()=>{
         //    document.getElementsByClassName('msg_history')[0].scrollBy(0,document.getElementsByClassName('msg_history')[0].scrollHeight)
         // },200);
         self.messagetype="";
         self.selectedFiles = null
         self.sendingChatMessage = false;
      },
      function (err) {
         self.selectedFiles = null;
         self.sendingChatMessage = false;
      });
   }
   else {

      this.chatlist.get(""+this.selectedid).messages.push(mMessage);
      var ref=(this.db.list('/users/'+this.currentuser.Id+'/message').push(mMessage));
      var refx=(this.db.list('/users/'+this.selectedid+'/message').push(mMessage));
      this.jservices.sendmessagenotification(this.selectedid+"").subscribe(data=>{
        console.log(data)
      })
      setTimeout(()=>{
         document.getElementsByClassName('msg_history')[0].scrollBy(0,document.getElementsByClassName('msg_history')[0].scrollHeight)
      },200);
      this.messagetype="";
      this.sendingChatMessage = false;
   }
  }
  
   selectAttachmentFile(event) {
      this.selectedFilesError = "";
      this.selectedFiles = event.target.files.item(0);
      console.log("size : " + this.selectedFiles["size"]);
      if(Number(this.selectedFiles["size"])/(1024 * 1024) > 10) {
         this.selectedFiles = null;
         this.selectedFilesError = "File size must under 10 MB."
      }
   }

   isImageFile(filename) {
      var fileExtension  = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
      var extention = filename.split('.').pop().toLowerCase();
      if(fileExtension.indexOf(extention) == -1) {
         return false;
      }
      return true;
   }

   insertTextAtIndices(text, obj) {
      return text.replace(/./g, function(character, index) {
      return obj[index] ? obj[index] + character : character;
      });
   }

   parseChatMessage(message) {
      //var urlMatches = message.match(/(http|ftp|https):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?/gi) || [];
      //var urlMatches = message.match(/\b(http|https|www.?)?:\/\/\S+/gi) || [];
      //var urlMatches = message.match(/((\b(http|https|www.?)?:\/\/)|(www))([^\r\n\t\f\v ,]+)/gim) || [];
      
      //urlMatches.forEach(link => {
      //   const startIndex = message.indexOf(link);
      //   const endIndex = startIndex + link.length;
      //   message = this.insertTextAtIndices(message, {
      //   [startIndex]: `<a href="${link}" target="_blank" rel="noopener noreferrer" class="embedded-link">`,
      //   [endIndex]: "</a>"
      //   });
      //});
      
      //var urlMatches = message.match(/(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])/gim) || [];
      var webPatern = /(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])/gim;
      var searchExp = new RegExp(webPatern)
      message = message.replace(searchExp, 
         str =>  "<a href='"+this.getHttpLink(str)+"' target='_blank' rel='noopener noreferrer' class='embedded-link'>" + str + "</a>");

      var emailPatern = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/gim
      searchExp = new RegExp(emailPatern)
      message = message.replace(searchExp, 
         str =>  "<a href='mailto:"+str+"'  rel='noopener noreferrer' class='embedded-emaillink'>" + str + "</a>");

      return message;
   }

   getHttpLink(link:string) {
      link = link.trim()
      if(link.length > 0 && link.indexOf("http://") == -1 && link.indexOf("https://") == -1) {
         link = "http://" + link;
      }
      return link;
   }





  goprofile(id) {
    this.router.navigate(['/dashbord', {outlets: {dashboardContent: ['profileview']}}], {queryParams: {id: id}});
  }
  addgig(){
    this.router.navigate(['/addjob']);

  }
  hiretalent(){
    this.router.navigate(['/dashbord']);
  }
}
class Messagelist{
  name:string;
  id:number;
  pic:string;
  messages:Message[];
  totreceived:number;
  lasttime:number;
  nbnew:number;
}
class Message{
  attachmentServerImagePath: string
  attachmentSize: string
  imageThumbnail: string
  mReceiverId: number
  mSenderUserId: number
  message: string
  messageType: string
  receiverAvtarURL: string
  receiverName: string
  senderAvtarUrl: string
  senderName: string
  sent: boolean
  timeStamp: number
}