import { Component, OnInit } from '@angular/core';
import { JugglrServices } from '../../../../Services/JugglrServices';
import { Router } from "@angular/router";
import * as moment from 'moment';
//import { clone } from 'node_modules_test/@firebase/storage/dist/src/implementation/object';
@Component({
  selector: 'app-notificationlist',
  templateUrl: './notificationlist.component.html',
  styleUrls: ['./notificationlist.component.css']
})
export class NotificationlistComponent implements OnInit {
  notifications: any = [];
  pagenum=1;
  totalnum=0;
  profile ;
  constructor(private jservices: JugglrServices, private router: Router) { }

  ngOnInit() {
    this.profile = JSON.parse(localStorage.getItem('currentUser'));
    this.getnotifications();
    
  }
  getnotifications(){
    this.jservices.currentnotificationdata.subscribe(notificationdata => {
      if (notificationdata['totalRecords'] > 0){
        this.notifications = [];
        notificationdata['notification'].forEach(element => {
          this.notifications.push(element)
        });
      }
    });
    /*this.jservices.getPendingNotification(this.profile['id'].toString(), this.profile['user_token'], this.profile['device_type'],this.pagenum)
    .subscribe(data => {
      if (data['status'] != null){
        data['data']['notification'].forEach(element => {
          this.notifications.push(element)
        });
        if(this.pagenum< data['data']['totalpages']){
          this.pagenum++;
          this.getnotifications();
        }
      }
    });*/
  }
  timeSince(date) {
    return moment(date).fromNow();
  }

  go(data) {
    this.notifications.forEach((element,index) => {
      if(element['data']['redirect'] == "CHAT" && data['redirect'] == 'CHAT') {
        this.router.navigate(['/dashbord', { outlets: { dashboardContent: ['chats'] } }]);
      }

      if(element['data']['redirect'] == "USERPROFILE" && data['redirect'] == 'USERPROFILE' && data['params']['user_id'] == element['data']['params']['user_id']) {
        this.jservices.updatenotificationreadflag(element['id']+"").subscribe(data=>{
          if (data['status'] != null){
            let notificationfinaldatalist = {};
            this.notifications.splice(index,1);
            notificationfinaldatalist['notification'] = this.notifications;
            notificationfinaldatalist['totalRecords'] = this.notifications.length;
            this.jservices.updatenotificationreaddata(notificationfinaldatalist);
            this.router.navigate(['/dashbord', { outlets: { dashboardContent: ['profileview'] } }], { queryParams: { id: data['params']['user_id'] } });
          }
        });
      }

      if(element['data']['redirect'] == "JOBDETAIL" && data['redirect'] == 'JOBDETAIL' && data['params']['job_id'] == element['data']['params']['job_id']) {
        this.jservices.updatenotificationreadflag(element['id']+"").subscribe(data=>{
          if (data['status'] != null){
            let notificationfinaldatalist = {};
            this.notifications.splice(index,1);                
            notificationfinaldatalist['notification'] = this.notifications;
            notificationfinaldatalist['totalRecords'] = this.notifications.length;
            this.jservices.updatenotificationreaddata(notificationfinaldatalist);
            this.router.navigate(['/dashbord', { outlets: { dashboardContent: ['jobdetails'] } }],{ queryParams: { id: data['params']['job_id']} });
          }
        });
      }
    });

    /*
    if (data['redirect'] == 'CHAT') {
      this.router.navigate(['/dashbord', { outlets: { dashboardContent: ['chats'] } }]);
    }
    else if (data['redirect'] == 'USERPROFILE') {
      this.router.navigate(['/dashbord', { outlets: { dashboardContent: ['profileview'] } }], { queryParams: { id: data['params']['user_id'] } });
    }
    else if (data['redirect'] == 'JOBDETAIL') {
      this.router.navigate(['/dashbord', { outlets: { dashboardContent: ['jobdetails'] } }],{ queryParams: { id: data['params']['job_id']} });
    }
    */
  }

}