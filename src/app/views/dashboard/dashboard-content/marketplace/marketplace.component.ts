import { Component, OnInit } from '@angular/core';
import { JugglrServices } from '../../../../Services/JugglrServices';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { User } from '../../../../models/user';
import { Observable } from 'rxjs';
import { catchError, debounceTime, switchMap, tap } from 'rxjs/operators';

@Component({
   selector: 'app-marketplace',
   templateUrl: './marketplace.component.html',
   styleUrls: ['./marketplace.component.css']
})
export class MarketplaceComponent implements OnInit {

   jobs = [];
   bpage: number = 1;
   ppage: number = 1;
   ppageTotal: number = 0;
   message: string = '';
   page: number = 1;
   pageTotal: number = 1;
   type: number = 0;
   filter: any[] = [{ id: '1', name: 'all jobs' }, { id: '2', name: 'my jobs' }, { id: '3', name: 'saved jobs' }, { id: '4', name: 'assigned jobs' }];
   filtertext: string = '1';
   textsearch: string;
   autotextsearch: string;
   textsearchicon: string = 'fa-search';   
   usersprofiles = [];
   businessarround = [];
   userProfessional=[];
   blnSearched:boolean = false;
   valuesearch = [
      { id: 56, searchtext: "Sales & Marketing" },
      { id: 43, searchtext: "ADMIN SUPPORT" },
      { id: 55, searchtext: "Web design" },
      { id: 69, searchtext: "Customer service" },
      { id: 58, searchtext: "Photography" },
      { id: 52, searchtext: "Website & app testing" },
      { id: 54, searchtext: "Graphic design" },
      { id: 49, searchtext: "Social media marketing" },
      { id: 42, searchtext: "Bookkeeping & accounting" },
      { id: 50, searchtext: "Content creation & comms" },
   ];
   valuesearch_users:any = [];
   constructor(private jservices: JugglrServices, private router: Router, private route: ActivatedRoute, private currentuser: User) { }

   ngOnInit() {
      let activeTabID: string = '';
      let profileDetailsBack = localStorage.getItem("dashboard-tabonback");
      localStorage.removeItem("dashboard-tabonback");
      this.route
         .queryParams
         .subscribe(params => {
            this.type = params['b'] != null ? parseInt(params['b']) : 0;
            activeTabID = params['tab'] != null ? params['tab'] : '';
            if (profileDetailsBack != null && profileDetailsBack.length > 0) {
               activeTabID = profileDetailsBack;
            }
            this.setActiveTabByID(activeTabID)
            console.log("type Value : " + this.type + " , Active Tab: " + activeTabID);
            //if (this.type==0){
            //  window.document.getElementById('a1').classList.add("active");
            //  window.document.getElementById('a2').classList.remove("active");
            //}
         });

      const profile = JSON.parse(localStorage.getItem('currentUser'));
      this.currentuser.myconstructor(profile);

      this.textsearch = '';
      if (this.loadTabDataByTabID(activeTabID) == false) {
         this.getlist();
      }
      this.getlistofusersthatcanhelp();
      this.getlistofbusinessarround();

   }

   getlistofbusinessarround() {
      this.jservices.getlistbusinessesarround(this.bpage).subscribe(data => {
         var tot = 0
         if (data['status']) {
            console.log(data['data']['users'])

            data['data']['users'].forEach(element => {
               for (var i = 0; i < element['userData'].length; i++) {
                  this.businessarround.push(element['userData'][i])
                  tot++
               }
            });
         }
         console.log("+++++++++++")
         console.log(this.businessarround)
         if (tot == 10) {
            this.bpage++
            if (this.bpage <= data['data']['totalpages'])
               this.getlistofbusinessarround()
         }
      })
   }


   getlistofusersthatcanhelp() {
      for (let k = 0; k < this.valuesearch.length; k++) {
         this.jservices.getresultfromfire(this.valuesearch[k].id + '').subscribe(data => {
            this.jservices.getlistusers('' + data, this.valuesearch[k].searchtext).subscribe(data => {
               console.log(data)
               var us = []
               for (let i = 0; i < data['data']['users'].length; i++) {
                  for (let j = 0; j < data['data']['users'][i]['userData'].length; j++) {
                     var usersp = data['data']['users'][i]['userData'][j];
                     usersp["suburb"] = data['data']['users'][i]['suburb'];
                     if (us.length < 10)
                        us.push(usersp);
                  }
               }
               if (us.length != 0)
                  console.log("users111",us);
                  this.valuesearch[k]['users'] = us;
            })
         });
      }

   }
   goDetail(id: string) {
      localStorage.removeItem("dashboard-tabonback");
      let tabID = this.getActiveTabID();
      this.router.navigate(['/dashbord', { outlets: { dashboardContent: ['jobdetails'] } }], { queryParams: { id: id, tab: tabID } });
   }
   timeSince(date) {
      return moment(date).fromNow();
   }

   savejob(id: number, i: number) {
      this.jobs[i]['is_savedjob'] = this.jobs[i]['is_savedjob'] == 0 ? 1 : 0;
      const type: string = this.jobs[i]['is_savedjob'] == 0 ? 'UNSAVE' : 'SAVE';
      this.jservices.savejob(this.jobs[i]['job_id'], this.currentuser, type)
         .subscribe(data => {
            if (data['status'] == 1)
               console.log(data);
            else {
               this.message = data['message'];
            }
         });
   }

   goUserDetail(userId) {
      let tabID = this.getActiveTabID();
      localStorage.setItem("dashboard-tabonback", tabID)
      this.router.navigate(['/dashbord', { outlets: { dashboardContent: ['profileview'] } }], { queryParams: { id: userId } });
   }

   offer() {
      this.mygig = false;

      this.filtertext = '1';
      this.page = 1;
      this.type = 0;
      window.document.getElementById('a2').classList.add("active");
      window.document.getElementById('a1').classList.remove("active");
      window.document.getElementById('a3').classList.remove("active");
      window.document.getElementById('a4').classList.remove("active");
      
      this.jservices.listJob(this.type + '', '1', '1', this.textsearch + '', '', '0-2000', this.page + '', "" + this.currentuser.latitude, "" + this.currentuser.longitude, "" + this.currentuser.Id, "" + this.currentuser.user_token, this.currentuser.device_type)
         .subscribe(data => {
           this.pageTotal = data['data']['totalpages']; 
            console.log(data)
            if (data['status'] == 1) {
               this.jobs = data['data']['jobs'];
               //this.nextpage()
            }
            else {
               this.message = data['message'];
            }
         });
   }
   need() {
      this.mygig = false;
      this.filtertext = '1';
      this.page = 1;
      this.type = 1;
      window.document.getElementById('a1').classList.add("active");
      window.document.getElementById('a2').classList.remove("active");
      window.document.getElementById('a3').classList.remove("active");
      window.document.getElementById('a4').classList.remove("active");
      
      this.jservices.listJob(this.type + '', '1', '1', this.textsearch + '', '', '0-2000', this.page + '', "" + this.currentuser.latitude, "" + this.currentuser.longitude, "" + this.currentuser.Id, "" + this.currentuser.user_token, this.currentuser.device_type)
         .subscribe(data => {
            this.pageTotal = data['data']['totalpages']; 
            if (data['status'] == 1) {
               this.jobs = data['data']['jobs'];
               //this.nextpage();
            }
            else {
               this.message = data['message'];
            }
         });
   }
   mygig = false;
   mine() {
      this.mygig = true;
      this.filtertext = '2';
      this.page = 1;
      this.type = 0;
      window.document.getElementById('a3').classList.add("active");
      window.document.getElementById('a2').classList.remove("active");
      window.document.getElementById('a1').classList.remove("active");
      window.document.getElementById('a4').classList.remove("active");
      
      this.jservices.listJob(this.type + '', '2', '1', this.textsearch + '', '', '0-2000', this.page + '', "" + this.currentuser.latitude, "" + this.currentuser.longitude, "" + this.currentuser.Id, "" + this.currentuser.user_token, this.currentuser.device_type)
         .subscribe(data => {
            this.pageTotal = data['data']['totalpages']; 
            if (data['status'] == 1) {
               this.jobs = data['data']['jobs'];
               //this.nextpage();
            }
            else {
               this.message = data['message'];
            }
         });
   }
   professionals() {
      this.mygig = false;

      this.filtertext = '1';
      this.ppage= 1;
      this.type = -1;
      this.userProfessional = [];
      window.document.getElementById('a2').classList.remove("active");
      window.document.getElementById('a1').classList.remove("active");
      window.document.getElementById('a3').classList.remove("active");
      window.document.getElementById('a4').classList.add("active");

      this.getListViewDataProfessional();
   }

   getListViewDataProfessional() {
      //getlistbusinessesarround
      this.ppage = 1;
      this.userProfessional=[];
      
      this.jservices.getListViewDataProfessional(this.ppage,this.textsearch + '').subscribe(data => {
         this.ppageTotal = data['data']['totalpages'];
         console.log("this.ppageTotal", this.ppageTotal);
         if (data['status']) {
            console.log(data['data']['users'])
            data['data']['users'].forEach(element => {
               for (var i = 0; i < element['userData'].length; i++) {
                  this.userProfessional.push(element['userData'][i])
               }
            });
         }
         console.log("+++++++++++")
         console.log(this.userProfessional)
      });
   }

   getProfessionalNextPage() {
      if (this.ppage < this.ppageTotal) { 
         this.ppage++
         
         this.jservices.getListViewDataProfessional(this.ppage,this.textsearch + '').subscribe(data => {
            this.ppageTotal = data['data']['totalpages'];
            if (data['status']) {
               console.log(data['data']['users'])
               data['data']['users'].forEach(element => {
                  for (var i = 0; i < element['userData'].length; i++) {
                     this.userProfessional.push(element['userData'][i])
                  }
               });
            }
            console.log("+++++++++++")
            console.log(this.userProfessional)
         });
      }
   }

   nextpage() {
      if(this.type == -1) {
         let id = this.getActiveTabID();
         if(id == "a1" || id == "a3") {
            this.type = 0;
         }
         else if(id == "a2") {
            this.type = 1;
         }
      }

      if ((this.jobs.length % 10 == 0) && (this.jobs.length > (this.page - 1) * 10)) {
         this.page += 1;
         
         this.jservices.listJob(this.type + '', this.filtertext, '1', this.textsearch + '', '', '0-2000', this.page + '', "" + this.currentuser.latitude, "" + this.currentuser.longitude, "" + this.currentuser.Id, "" + this.currentuser.user_token, this.currentuser.device_type)
            .subscribe(data => {
               if (data['status'] == 1) {
                  for (let entry of data['data']['jobs']) {
                     this.jobs.push(entry);
                  }
                  console.log("this.nextpage() called" + this.page);
                  //this.nextpage();
               }
            });
      }
   }
   setfilter(ss: string) {
      if (this.filtertext != ss) {
         this.filtertext = ss;
         this.page = 1;
         this.getlist();
      }
   }
   getlist() {
      if(this.textsearch != "") {
         this.blnSearched = true;
      }
      else {
         this.blnSearched = false;
      }
      if(this.type != -1) {
         this.jservices.listJob(this.type + '', this.filtertext, '1', this.textsearch + '', '', '0-2000', this.page + '', "" + this.currentuser.latitude, "" + this.currentuser.longitude, "" + this.currentuser.Id, "" + this.currentuser.user_token, this.currentuser.device_type)
         .subscribe(data => {
            if (data['status'] == 1) {
               this.jobs = data['data']['jobs'];
               //this.nextpage();
            }
            else {
               this.message = data['message'];
            }
         });
      }
      else {
         this.professionals();
      }
   }
   search = (text$: Observable<string>) =>
   text$.pipe(
      debounceTime(300),
      tap(() => {
         console.log(this.autotextsearch);
         this.textsearchicon = 'fa-search';
      }),
      switchMap(term => term.length > 2 ?
         this.jservices.autosuggestionsearch(term,1) : []
      )
   )

   formatter = (x: { search_index: string }) => x.search_index + "";

   inputformatter = (x: { search_index: string, search_value: string}) => {
      this.textsearch = x.search_index ? x.search_index + "" : "";
      this.autotextsearch = x.search_index;
      this.changesearch();
      return x.search_index ? x.search_index + "" : "";
   };

   clearsearch(ele) {
      if(this.textsearchicon == 'fa-times-circle')
      {
         (<HTMLInputElement>window.document.getElementsByName('textsearch')[0]).value = '';
         this.textsearch = '';
         this.textsearchicon = 'fa-search';
         setTimeout(()=>{
            this.autotextsearch = '';
            this.changesearch();
         }, 200);         
      }
      else {
         this.changesearch();
      }
      
   }
   changesearch() {
      if(typeof(this.autotextsearch['search_index']) == 'undefined')
      {
         this.textsearch = this.autotextsearch;
      }
      else if(typeof(this.autotextsearch['search_index']) != 'undefined')
      {
         this.textsearch = this.autotextsearch['search_index'];
      }

      if((<HTMLInputElement>document.getElementsByName('textsearch')[0]).value != '')
      {
         this.textsearchicon = 'fa-times-circle';
      }
      this.page = 1;
      this.getlist();
      this.getSearchedUser();
   }
   moreusers(i) {
      if ((Math.floor(i / 3) < this.valuesearch.length)) {
         if (this.valuesearch[Math.floor(i / 3)]['users'] != null) {
            if (i % 3 == 0) {
               return true;
            }
            else {
               return false;
            }
         }
         else {
            return false;
         }
      }
      else {
         return false;
      }

   }
   morebusinesesusers(i) {
      if ((Math.floor(i / 3) * 10 < this.businessarround.length)) {
         if (i % 3 == 0) {
            return true;
         }
         else {
            return false;
         }
      }
      else {
         return false;
      }

   }
   businessarroundfragment(indx) {
      let fragmentlist = [];
      for (let i = indx * 10; i < (indx * 10 + 10); i++) {
         fragmentlist.push(this.businessarround[i])
      }
      return fragmentlist;
   }


   lookfor(indx) {
      console.log(indx)
      this.router.navigate(['/dashbord', { outlets: { dashboardContent: ['pws'] } }], { queryParams: { id: indx.id, searchtext: indx.searchtext } });

   }

   getActiveTabID() {
      if (window.document.getElementById('a1').classList.contains("active")) {
         return "a1";
      }
      else if (window.document.getElementById('a2').classList.contains("active")) {
         return "a2";
      }
      else if (window.document.getElementById('a3').classList.contains("active")) {
         return "a3";
      }
      else if (window.document.getElementById('a4').classList.contains("active")) {
         return "a4";
      }
      
   }
   setActiveTabByID(id) {
      window.document.getElementById('a1').classList.remove("active");
      window.document.getElementById('a2').classList.remove("active");
      window.document.getElementById('a3').classList.remove("active");
      window.document.getElementById('a4').classList.remove("active");

      if (window.document.getElementById('id')) {
         window.document.getElementById('id').classList.add("active");
      }
      else {
         window.document.getElementById('a2').classList.add("active");
      }
   }
   loadTabDataByTabID(id): boolean {
      let retVal: boolean = true;
      if (id == "a1") {
         this.need();
      }
      else if (id == "a2") {
         this.offer();
      }
      else if (id == "a3") {
         this.mine()
      }
      else if (id == "a4") {
         this.professionals()
      }
      else {
         retVal = false;
      }
      return retVal;
   }

   getSearchedUser() {
      this.valuesearch_users =[];
      if(this.textsearch != "") {
         for(let i=0;i<this.valuesearch.length;i++) {
            if(this.valuesearch[i].searchtext.toLowerCase().includes(this.textsearch.toLowerCase())) {
               if (this.valuesearch[i]['users'] != null && this.valuesearch[i]['users'].length > 0) {
                  this.valuesearch_users.push(this.valuesearch[i]);
               }
            }
         }
      }
   }

   onPageScroll() {
      console.log("Page Scrolled");
      if(this.type == -1) {
         this.getProfessionalNextPage();
      }
      else {
         this.nextpage();
      }
   }
}
