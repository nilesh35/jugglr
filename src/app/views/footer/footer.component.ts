import { Component, OnInit, HostListener } from '@angular/core';

declare var $: any;
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  backToTop() {
    $('html,body').animate({scrollTop : 0} , 'slow');
  }

  @HostListener("window:scroll", []) onWindowScroll() {
    // when the window is scrolled
    const verticalOffset = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;

    if(verticalOffset > 0)
    {
      $('.back-to-top').show();
    }
    else{
      $('.back-to-top').hide();
    }
  }
}
