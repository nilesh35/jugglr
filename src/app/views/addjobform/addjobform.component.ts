import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { Job } from '../../models/job';
import { Router } from "@angular/router";
import { JugglrServices } from '../../Services/JugglrServices';
import { Observable } from 'rxjs';
import { debounceTime, switchMap, tap } from 'rxjs/operators';
declare var places: any;

@Component({
  selector: 'app-addjobform',
  templateUrl: './addjobform.component.html',
  styleUrls: ['./addjobform.component.css'],
  providers: [JugglrServices]
})
export class AddjobformComponent implements OnInit {
  @Input() isJobInfoSave: boolean = false;
  loading: boolean = false;

  TypeUser: boolean;
  model: any = '';
  strService: string = '';
  placesAutocomplete: any;
  message: string = '';
  constructor(private router: Router, private jservices: JugglrServices, public job: Job) {
    console.log(window.document.getElementById('address-input'))
  }


  ngOnInit() {
    this.TypeUser = (localStorage.getItem('typeUser') == '1') ? true : false;
    this.placesAutocomplete = places({
      container: window.document.querySelector('#address-input'),
      countries: 'au'
    });
    this.placesAutocomplete.on('change', e => this.job.Location = (e.suggestion.value));
    //this.model = "Hello";
    if(this.isJobInfoSave) {
        const jobinfo = JSON.parse(localStorage.getItem('jobInfo'));
        if(jobinfo != null) {
            this.job = jobinfo;
            console.log("Saved job :: " , this.job);
            this.model = { "search_index": this.job.Customservice, "search_value": "SUBSERVICE-"+this.job.Service_id+"-" + this.job.Subservice_id };
            if(this.job.Datetime) {
                let dt:Date = new Date(this.job.Datetime);
                this.job.Datetime = dt.getDate() + "/" + (dt.getMonth()+1) + '/' + dt.getFullYear();
            }
            console.log("Job Retrieved", this.job);
        }
    }
    else {
        this.job = new Job();
    }
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      tap(() => console.log(this.model)),
      switchMap(term => term.length > 2 ?
        this.jservices.autosuggestionsearch(term,'2') : []
      )
    )

  formatter = (x: { search_index: string }) => x.search_index + "";


  inputformatter = (x: { search_index: string, search_value: string }) => {
    this.strService = x.search_value
    console.log(this.model)
    return x.search_index ? x.search_index + "" : "";
  };

  save() {
    
    this.loading = true;
    const profile = JSON.parse(localStorage.getItem('currentUser'));
    this.verifyService();
    console.log(profile);
    if (profile != null) {
      this.job.User_id = profile['id'].toString();
      this.job.User_token = profile['user_token'];
      this.job.Device_type = profile['device_type'];
      if(this.job.Datetime ) {
        let datePart:Date;
        if(typeof this.job.Datetime == "object") {
            datePart = new Date(this.job.Datetime);
            //this.job.Datetime = datePart.getDate() + "/" + (datePart.getMonth()+1) + '/' + datePart.getFullYear();
        }
        else {
            var dateParts = this.job.Datetime.split('/'); //DD/MM/YYYY
            datePart = new Date(+Number(dateParts[2]), Number(dateParts[1]) - 1, +Number(dateParts[0]));   
        }

        //this.job.Datetime = datePart.toString();
        //2019-07-11T12:00" 
        this.job.Datetime = datePart.getFullYear() + "-" + ("0" + (datePart.getMonth()+1)).slice(-2) + "-" + ("0" + datePart.getDate()).slice(-2) + "T12:00";
      }
      
      console.log(this.job.Datetime);
      //localStorage.setItem['jobInfo']=this.job;
      //if (this.isJobInfoSave) {
        this.jservices.addJob(this.job)
          .subscribe(data => {
            this.isJobInfoSave = false

            console.log("Job Info Saved");
            if (data['status'] == 1)
              this.router.navigate(['congratulation']);
            else
              this.message = data['message'];
            this.loading = false;
          });
      //} 
      //else {
      //  this.router.navigate(['loginBusiness']);
      //}
    } 
    else {
      if (this.TypeUser) {
        this.router.navigate(['sociallogin'], { queryParams: { from: 'addjob' } });
      }
      else 
      {
          if(this.job.Datetime) {
            this.job.Datetime = this.job.Datetime.toString();
          }
        
        localStorage.setItem('jobInfo', JSON.stringify(this.job));
        this.router.navigate(['loginBusiness']);
      }
    }
    console.log(this.job);
  }

  verifyService() {
    if (this.strService == null) {
        if(typeof this.model == "object") { 
            this.job.Customservice = this.model["search_index"];
        }
        else {
            this.job.Customservice = this.model;
        }
     
      this.job.Service_id = '';
      this.job.Subservice_id = '';
      this.job.Subservicetag_id = '';

    } else {
      const x = this.strService.split("-");
      console.log("x= " + x);
      //this.job.Customservice = '';
      if(typeof this.model == "object") { 
            this.job.Customservice = this.model["search_index"];
        }
        else {
            this.job.Customservice = this.model;
        }
        
      if (x.length == 3) {
        this.job.Service_id = x[1];
        this.job.Subservice_id = x[2];
      } else {
        this.job.Service_id = x[1];
        this.job.Subservice_id = x[2];
        this.job.Subservicetag_id = x[3];
      }
    }
  }
  
}
