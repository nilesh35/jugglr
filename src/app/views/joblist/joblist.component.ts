import { Component, OnInit } from '@angular/core';
import { JugglrServices } from '../../Services/JugglrServices';

@Component({
  selector: 'app-joblist',
  templateUrl: './joblist.component.html',
  styleUrls: ['./joblist.component.css'],
  providers:[JugglrServices]
})
export class JoblistComponent implements OnInit {
  jobs=[];
  constructor(private jservices: JugglrServices) { }

  ngOnInit() {
    const profile = JSON.parse(sessionStorage.getItem('currentUser'));
    const userid = profile['id'].toString();
    const usertoken = profile['user_token'];
    const devicetype = profile['device_type'];
    this.jservices.listJob('0','1','1','','','0-50','1','-30.8','111',userid,usertoken,devicetype)
        .subscribe(data => {
           console.log(data);
           this.jobs = data['data']['jobs'];
           console.log(this.jobs);
        });
  }

}
