import { Component, OnInit } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ModalConfigComponent} from '../../../utils/modal-config/modal-config.component';

declare var $: any;
@Component({
  selector: 'app-jobsstatic',
  templateUrl: './jobsstatic.component.html',
  styleUrls: ['./jobsstatic.component.css']
})
export class JobsstaticComponent implements OnInit {

  constructor(private modal:NgbModal) { }

  ngOnInit() {
    $('html,body').animate({scrollTop : 0} , 'slow');
  }
signin(){
  localStorage.removeItem('from');
  this.modal.open(ModalConfigComponent, { centered: true });

}
}
