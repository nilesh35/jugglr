import { Injectable } from '@angular/core';

@Injectable()
export class Offer { 
    user_id:string=''
    user_with:string=''
    need_serviceid:string=''
    need_subserviceid:string=''
    need_customservice:string=''
    offer_serviceid:string=''
    offer_subserviceid:string=''
    offer_customservice:string=''
    need_datetime:string=''
    need_location:string=''
    need_note:string=''
    offer_datetime:string=''
    offer_location:string=''
    offer_note:string=''
    amount:string=''
    discount:string=''
    need_subservicetagid:string=''
    offer_subservicetagid:string=''

}
